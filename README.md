# DialogueDwarf - Your Mythical Chatsmith

The # DialogueDwarf -  is a versatile script written in C# for the Godot game engine that facilitates the implementation of dynamic and interactive dialogues in your games. DialogueDwarf has features such as dialogue creation, JSON-defined dialogues, branching dialogues, and conditional dialogues, it empowers game developers to create immersive narrative experiences effortlessly.

==== Features ====

  *  Dialogue Creation: Easily create and manage dialogues within the Godot engine, providing a streamlined workflow for narrative design
  *  JSON-Defined Dialogues: Define your dialogues using JSON format, allowing for flexibility and ease of integration with external tools or editors.
  * Branching Dialogues: Create non-linear narratives by implementing branching dialogues. Guide the story based on player choices, leading to diverse outcomes.
  * Conditional Dialogues: Tailor dialogues dynamically based on in-game conditions. Respond to player actions or environmental variables to enhance the depth of your narrative.

==== Usage ====

    Integration:
        Add the DialogDwarf folder to your Godot project.
        Configure the script parameters in the Godot editor to suit your project's needs.

    Dialogue Creation:
        Use the provided API to create dialogues directly in your game scenes.
        Alternatively, define dialogues in JSON format for more extensive and external editing capabilities.

    Branching and Conditions:
        Utilize the script functions to implement branching dialogues.
        Apply conditional statements to customize dialogues based on specific in-game situations.



## Getting started


## Project status
Just startet
